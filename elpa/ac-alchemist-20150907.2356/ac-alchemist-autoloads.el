;;; ac-alchemist-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "ac-alchemist" "ac-alchemist.el" (23197 34494
;;;;;;  541249 100000))
;;; Generated autoloads from ac-alchemist.el

(autoload 'ac-alchemist-setup "ac-alchemist" "\


\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; ac-alchemist-autoloads.el ends here
